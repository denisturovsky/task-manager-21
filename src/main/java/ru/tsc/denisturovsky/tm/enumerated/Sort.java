package ru.tsc.denisturovsky.tm.enumerated;

import ru.tsc.denisturovsky.tm.comparator.CreatedComparator;
import ru.tsc.denisturovsky.tm.comparator.DateBeginComparator;
import ru.tsc.denisturovsky.tm.comparator.NameComparator;
import ru.tsc.denisturovsky.tm.comparator.StatusComparator;
import ru.tsc.denisturovsky.tm.exception.field.SortIncorrectException;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

public enum Sort {

    BY_NAME("Sort by Name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_BEGIN("Sort by date begin", DateBeginComparator.INSTANCE);

    private final Comparator comparator;
    private String displayName;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        Optional<Sort> sortOptional = Arrays.stream(values()).filter(m -> m.name().equals(value)).findFirst();
        if (sortOptional.orElse(null) == null) throw new SortIncorrectException();
        return sortOptional.orElse(null);
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
