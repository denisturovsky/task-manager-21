package ru.tsc.denisturovsky.tm.enumerated;

import ru.tsc.denisturovsky.tm.exception.field.SortIncorrectException;
import ru.tsc.denisturovsky.tm.exception.field.StatusEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.StatusIncorrectException;

import java.util.Arrays;
import java.util.Optional;

public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) throw new StatusEmptyException();
        Optional<Status> statusOptional = Arrays.stream(values()).filter(m -> m.name().equals(value)).findFirst();
        if (statusOptional.orElse(null) == null) throw new StatusIncorrectException();
        return statusOptional.orElse(null);
    }

    public String getDisplayName() {
        return displayName;
    }

}
