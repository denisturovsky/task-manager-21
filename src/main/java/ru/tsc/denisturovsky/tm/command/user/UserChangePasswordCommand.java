package ru.tsc.denisturovsky.tm.command.user;

import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "user-change-password";

    public static final String DESCRIPTION = "Change user password";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        String userId = getAuthService().getUserId();
        System.out.println("[ENTER NEW PASSWORD:]");
        String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}
